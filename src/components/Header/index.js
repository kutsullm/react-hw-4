import React from 'react'
import { AiOutlineShoppingCart, AiOutlineHeart, AiOutlineHome } from "react-icons/ai"
import { StyledHeader } from "./styled";
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
export const Header = () => {
    const cart = useSelector(state => Object.values(state.cart?.items))
    const favInStorage = useSelector(state => Object.values(state.products)) || []
    const fav = favInStorage.filter(item => item.isInFav)
    const count = cart.reduce((acc, curr) => {
        acc += curr.count
        return acc
    }, 0)
    return (
        <StyledHeader >
            <nav>
                <ul>
                    <li><Link to="/"><AiOutlineHome /></Link></li>
                    <li><Link to="/favorite"><AiOutlineHeart /> - {fav.length}</Link></li>
                    <li><Link to="/cart"><AiOutlineShoppingCart /> - {count}</Link></li>
                </ul>
            </nav>
        </StyledHeader>
    )
}
