import Button from "../Button";
import { useSelector, useDispatch } from "react-redux";
import { StyledModal, ModalBckg, ModalContent, StyledParagraph, StyledHeader, StyledH, StyledButton } from "./styled";
import PropTypes from "prop-types";
import { addToCart, deleteFromCart, hideModal } from "../../store/action";

const Modal = () => {
  const configModal = {
    addToCart: {
      id: 0,
      title: 'Do you want to add to cart?',
      text: 'Do you want to add to cart?',
      hasCloseButton: true,
      onSubmit() {
        const storage = JSON.parse(localStorage.getItem('cart'))
        const finded = storage.find(item => item.id === statesCurrentId)
    
        if (finded) {
          let counter = finded.count + 1
          const findedIndex = storage.findIndex((item) => item.id === statesCurrentId)
          const inCart = storage.toSpliced(findedIndex, 1, { id: statesCurrentId, count: counter })
          localStorage.setItem("cart", JSON.stringify(inCart))
          dispatch(addToCart(statesCurrentId, counter))
        }
        if (!finded) {
          localStorage.setItem("cart", JSON.stringify([...storage, { id: statesCurrentId, count: 1 }]))
          dispatch(addToCart(statesCurrentId))
        }
        dispatch(hideModal())
      }
    },
    removeFromCart: {
      id: 1,
      title: 'Do you want to delete from cart?',
      text: 'Do you want to delete from cart?',
      hasCloseButton: true,
      onSubmit() {
        const storage = JSON.parse(localStorage.getItem('cart'))
        const finded = storage.find(item => item.id === statesCurrentId)
        if (finded) {
          const findedIndex = storage.findIndex((item) => item.id === statesCurrentId)
          const inCart = storage.toSpliced(findedIndex, 1)
          localStorage.setItem("cart", JSON.stringify(inCart))
          dispatch(deleteFromCart(statesCurrentId))
        }
        dispatch(hideModal())
      }
    }
  }
  const modalName = useSelector(state => state.modal.id)
  const statesCurrentId = useSelector(state => state.cart.currentId)

  const dispatch = useDispatch()
  const { title, text, hasCloseButton, onSubmit } = configModal[modalName];

  const optionalCeneterd = {
    'justifyContent': hasCloseButton ? 'space-between' : 'center',
  }

  return (
    <ModalBckg onClick={() => dispatch(hideModal())} >
      <StyledModal onClick={e => e.stopPropagation()} >
        <StyledHeader style={optionalCeneterd}>
          <StyledH>{title}</StyledH>
          {hasCloseButton && <StyledButton onClick={() => dispatch(hideModal())} >&#10761;</StyledButton>}
        </StyledHeader>
        <ModalContent>
          <StyledParagraph>{text}</StyledParagraph>
          <div>
            <Button text='Ok' backgroundColor='#008CBA' onClick={onSubmit} />
            <Button text='Cancel' backgroundColor='#e7e7e7' onClick={() => dispatch(hideModal())} />
          </div>
        </ModalContent>
      </StyledModal>
    </ModalBckg>
  )
}

export default Modal;