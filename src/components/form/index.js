import React from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import { object, string, number } from 'yup';
import Button from '../Button';
import { useDispatch } from 'react-redux';
import { clearCart } from '../../store/action';
const BasicForm = () => {
    const dispatch = useDispatch()
    const OrderSchema = object({
        firstName: string()
            .min(2, "Too Short!")
            .max(50, "Too Long!")
            .required("Firstname is required"),

        lastName: string()
            .min(2, "Too Short!")
            .max(50, "Too Long!")
            .required("Lastname is required"),

        phone: string()
            .required("Phone number is required"),

        age: number().required().positive().integer(),

        address: string()
            .min(10, "Too Short!")
            .max(100, "Too Long!")
            .required("Address is required"),
    })
    return (
        <div>
            <h1>Make Order</h1>
            <Formik
                initialValues={{
                    firstName: '',
                    lastName: '',
                    phone: '',
                    address: "",
                    age: 18
                }}
                validationSchema={OrderSchema}
                onSubmit={async (values) => {
                    alert(JSON.stringify(values, null, 2));
                    localStorage.setItem('cart', JSON.stringify([]))
                    dispatch(clearCart())
                }}
            >
                <Form>
                    <div>
                        <label htmlFor="firstName">First Name
                            <Field id="firstName" name="firstName" placeholder="Jane" required />
                            <ErrorMessage
                                name="firstName"
                                component="span"
                                className="error"
                            />
                        </label>
                    </div>
                    <div>
                        <label htmlFor="lastName">Last Name
                            <Field id="lastName" name="lastName" placeholder="Doe" required />
                            <ErrorMessage
                                name="lastName"
                                component="span"
                                className="error"
                            />
                        </label>
                    </div>
                    <div>
                        <label htmlFor="phone">Phone
                            <Field
                                id="phone"
                                name="phone"
                                placeholder="380500000000"
                                required
                            />
                            <ErrorMessage
                                name="phone"
                                component="span"
                                className="error"
                            />
                        </label>
                    </div>
                    <div>
                        <label htmlFor="age">Age
                            <Field id="age" name="age" placeholder={18} type="number" required />
                            <ErrorMessage
                                name="age"
                                component="span"
                                className="error"
                            />
                        </label>
                    </div>
                    <div>
                        <label htmlFor="address">Address
                            <Field id="address" name="address" placeholder="Kyyiv, ..." required />
                            <ErrorMessage
                                name="address"
                                component="span"
                                className="error"
                            />
                        </label>
                    </div>
                    <Button type="submit" text="Submit" />
                </Form>
            </Formik>
        </div>
    );
}

export default BasicForm