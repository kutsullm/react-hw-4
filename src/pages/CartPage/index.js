import React from 'react'
import { Cart } from '../../components/cart'
import { useDispatch, useSelector } from 'react-redux'
import Button from '../../components/Button'
import { clearCart } from '../../store/action'
import BasicForm from '../../components/form'


export const CartPage = () => {
  const dispatch = useDispatch()
  const productsInCart = useSelector(state => state.cart.items) || []
  const products = useSelector(state => Object.values(state.products)) || []
  const ids = productsInCart.map(item => item.id)
  const productList = products.filter(item => ids.includes(item.id))
  const handleClear = () => {
    localStorage.setItem('cart', JSON.stringify([]))
    dispatch(clearCart())
  }
  return (
    <>
      <Button text="Clear cart" onClick={handleClear} />
      {productList.map((product) => {
        return <Cart key={product.id} {...product} />
      })}
      {!!productList.length && <BasicForm />}
    </>
  )
}
