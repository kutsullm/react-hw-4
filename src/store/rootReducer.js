import { combineReducers } from 'redux';
import productsReducer from './reducers/productsReducer';
import cartReducer from './reducers/cartReducer';
import modalReducer from './reducers/modalReducer';

const rootReducer = combineReducers({
  products: productsReducer,
  cart: cartReducer,
  modal: modalReducer
});

export default rootReducer